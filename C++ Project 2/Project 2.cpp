//
//  Project 2.cpp
//  C++ Homework
//
//  Created by Danniel Garnge on 12/3/21.
//

#include <iostream>
#include <math.h>

using namespace std;

// Define Integrand
float integrand(float x){
    float value;
    value = sqrt(1 - x*x);
    return value;
}

int main()
{
    // Get input number of sample points from user
    float N; //Initialize input as float
    cout << "Enter the number of intervals: N = ";
    cin >> N; // Get user input from the keyboard
    
    float dx = 1/N; //Divide by range length to get interval size
    float sum = 0; //Initialize Sum
    
    for (float i=0; i < 1 - dx; i+= dx){ //Loop over evenly spaced points
        sum += (integrand(i) + integrand(i + dx)); //Add term to sum
    }
    
    float pi_estimate = sum * dx * 2;
    
    cout << "Estimated Value of Pi: " << pi_estimate << endl; //Print pi estimate
    cout << "Approximation Error: " << abs(pi_estimate- M_PI) << endl; //Print Error
    
}
