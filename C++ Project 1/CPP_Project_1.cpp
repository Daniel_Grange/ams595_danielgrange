// C++ Project 1
// Daniel Grange
//
//  Created on 11/24/21.
//

#include <iostream>
#include <vector>

using namespace std;

// Define print vector function for question 2
void print_vector(const vector<int>& v) {
    for (int i : v){
        cout << i << " "; //Print each item of vector followed by space
    }
    cout << endl;
}

// Define isprime function for Q4a
bool isprime(int n) {
    bool result = false;
    for (int i = 2; i < n; i++) {
        result = result || (n % i == 0);
    }
    return result;
}

// Test cases for isprime function
void test_isprime() {
    std::cout << "isprime(2) = " << isprime(2) << '\n';
    std::cout << "isprime(10) = " << isprime(10) << '\n';
    std::cout << "isprime(17) = " << isprime(17) << '\n';
}

// Factorize Function for Q4b
vector<int> factorize(int n) {
    vector<int> answer = {}; //Initialize answer vector
    for (int i = 1; i <= n; i++) { //Loop over all smaller positive integers
        if (n % i == 0) {
            answer.push_back(i); //If divisible, add to answer vector
        }
    }
    return answer;
}
// Test Cases for Factorize Function
void test_factorize() {
    print_vector(factorize(2));
    print_vector(factorize(72));
    print_vector(factorize(196));
}

// Prime Factorization Function for Q4c
vector<int> prime_factorize(int n) {
    vector<int> answer = {};
    int i = 1;
    while (n > 1) { //Loop until completely factorized
        i++;
        if (n % i == 0) { //Check possible prime factor
            answer.push_back(i); //Append prime factor
            n = int (n / i); //Divide by factor
            i = 1; //Reinitialize factor guess
        }
    }
    return answer;
}
    
// Test Cases for Prime Factorization Function
void test_prime_factorize() {
    print_vector(prime_factorize(2));
    print_vector(prime_factorize(72));
    print_vector(prime_factorize(196));
}
    
// Pascal Triangle Function for Q5

void pascal_triangle(int n) {
    vector<int> row = {1};
    vector<int> row_new = {1};
    for(int i = 0; i <=n; i++) {
        print_vector(row);
        for(int j = 0; j <= i; j++) {
            row_new[j + 1] = row[j] + row[j + 1];
        }
        row_new.push_back(1); //Add 1 to row
        row = row_new;
    }
}

int main()
{
    // Question 1. Conditional Statements
    cout << "Question 1" << endl;
    int n;
    cout << "Enter a number: ";
    cin >> n; //Get input n
    
    //Begin Switch Statement
    switch (n){
        case -1:
            cout << "\nNegative One" << endl;
            break;
        case 0:
            cout << "\nZero" << endl;
            break;
        case 1:
            cout << "\nPositive One" << endl;
            break;
        default:
            cout << "\nOther Value" << endl;
    }
    
    // Question 2. Printing a Vector
    cout << "\nQuestion 2 with example vector" << endl;
    
    vector<int> vec = {1, 2, 3, 4, 5};
    
    print_vector(vec);
    
    // Question 3. While Loops
    cout << "\nQuestion 3 Fibonnaci Sequence" << endl;
    
    // Initialize Sequence
    int f_old = 1;
    int f_new = 2;
    
    // Loop until Fibonnaci Sequence Exceded 4 million
    while (f_new < 4000000) {
        f_new = f_old + f_new; //New term
        f_old = f_new - f_old; //Roundabout way to get back old term
        cout << f_old << endl; //Print
    }
    
    // Question 4. Function
    // Question 4a. If Prime
    cout << "\nQuestion 4a" << endl;
    
    test_isprime();
    
    // Question 4b. Factorize
    cout << "\nQuestion 4b" << endl;
    
    test_factorize();
    
    // Question 4c. Prime Factorize
    cout << "\nQuestion 4c" << endl;
    
    test_prime_factorize();
    
    // Question 5. Recursive Functions and Loops
    cout << "\nQuestion 5" << endl;
    
    pascal_triangle(5);

}
