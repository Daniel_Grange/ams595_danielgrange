function [pi_approx, iterations] = Monte_Carlo_Modified(precision)
%pi_approx is the approximation to pi
%iterations is the number of random points that are measured

    %Initialize Ledger
    hits = 0;
    
    %Initialize Iterations
    iterations = 0;
    
    %Initialize pi_approx
    pi_approx = 0;
    
    %Initialize Figure
    figure
    x = linspace(0,1);
    y = sqrt(1 - x.^2);
    plot(x,y)
    
   hold on

    %Begin Loop
    while abs(pi_approx - pi) > 10^(-1 * precision)
        %Increase Count by 1
        iterations = iterations + 1;
        
        %Generate Random Point in [0,1] x [0,1]
        random_point = rand(2,1);

        %Calculate how far the point is from 0
        dist_from_zero = norm(random_point);

        %If point is inside add to ledger of hits
        if dist_from_zero <= 1
            hits = hits + 1;
            %Add blue point if inside unit circle
            plot(random_point(1), random_point(2), 'o', 'Color', 'blue')
            hold on
        else
            %Add red point if outside unit circle
            plot(random_point(1), random_point(2), 'o', 'Color', 'red')
            hold on
      
        end
    
        %Estimate Pi based on ratio of points inside unit circle to total points
        pi_approx = 4 * (hits / iterations);
        title('Calculated value of Pi = ' + string(round(pi_approx, precision)))
        drawnow
    end
%Save and stop updating figure
saveas(gcf, 'scatter.png', 'png')
hold off

%Return the approximate value of Pi rounded to the desired precision
round(pi_approx, precision)
end
    
  
   