function pi_approx = Monte_Carlo_Simulation(iterations)
%pi_approx is the approximation to pi
%iterations is the number of random points that are measured
    %Initialize Ledger
    hits = 0;

    %Begin Loop
    for i = 1:iterations
        %Generate Random Point in [0,1] x [0,1]
        random_point = rand(2,1);

        %Calculate how far the point is from 0
        dist_from_zero = norm(random_point);

        %If point is inside add to ledger of hits
        if dist_from_zero <= 1
            hits = hits + 1;
        end
    end
    
%Estimate Pi based on ratio of points inside unit circle to total points
pi_approx = 4 * (hits / iterations);
end