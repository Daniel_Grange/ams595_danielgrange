%Run Simulation for various number of iterations
iteration_list = 2.^(10:20); %Iterate for various powers of 2 (1024 to 2^20)

n = length(iteration_list); %Amount simulations run

%Initialize Results
pi_values = zeros(1, n);
time_values = zeros(1, n);
true_values = pi * ones(1, n);

%Loop over each simulation
for i = 1:n
    tic
    pi_values(i) = Monte_Carlo_Simulation(iteration_list(i));
    time_values(i) = toc;
end

%Calculate error Values
error_values = abs(pi_values - true_values);

%Plots
figure
semilogx(iteration_list, pi_values, '-o', 'Color', 1/255 * [148 0 211], 'LineWidth', 3)
set(gca,'FontSize',14)
title('Apprixomated Values of \pi', 'FontSize', 18)
xlabel('Number of iterations', 'FontSize', 16)
ylabel('Approximated value', 'FontSize', 16)
saveas(gcf, 'pi_vs_iteration.png', 'png')

figure
yyaxis left
loglog(iteration_list, error_values, '-o', 'Color', 1/255 * [135,206,250], 'LineWidth', 3)
xlabel('Number of iterations', 'FontSize', 16)
ylabel('Error Value', 'FontSize', 16)

yyaxis right
loglog(iteration_list, time_values, '-o', 'Color', 1/255 * [255,127,80], 'LineWidth', 3)
title('Error and Time vs. Number of Iterations', 'FontSize', 18)
xlabel('Number of iterations', 'FontSize', 16)
ylabel('Time to run (seconds)', 'FontSize', 16)
legend('Approximation Error', 'Time to run')
saveas(gcf, 'computational_cost.png', 'png')