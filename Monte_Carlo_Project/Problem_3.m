%Run Simulation for various number of iterations
precision_list = 1:4; %Iterate for significant figures 1 through 4

n = length(precision_list); %Amount simulations run

%Initialize Results
pi_values = zeros(1, n);
time_values = zeros(1, n);
iterations = zeros(1,n);

%Loop over each simulation
for i = 1:n
    tic
    [pi_values(i), iterations(i)]  = Monte_Carlo_Modified(precision_list(i));
    time_values(i) = toc;
end

%Calculate error Values
error_values = abs(pi_values - pi);

%Plots
figure
yyaxis left
semilogy(precision_list, iterations, '-o', 'Color', 1/255 * [135,206,250], 'LineWidth', 3)
title('Iterations needed for precision values')
xlabel('Significant Digits')
ylabel('Number of iterations')

yyaxis right
semilogy(precision_list, time_values, '-o', 'Color', 1/255 * [255,127,80], 'LineWidth', 3)
ylabel('Computation Time (seconds)')
legend('Number of Iterations', 'Computation Time')
saveas(gcf, 'problem_3.png', 'png')