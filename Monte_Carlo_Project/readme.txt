README:
The Monte Carlo Pi Simulator project code is split into 6 .m Matlab files. The
first three are functions corresponding to different implementations of a Monte
Carlo Simulation of Pi, labeled 'Monte_Carlo_Simulation.m',
'Monte_Carlo_Precision.m', and 'Monte_Carlo_Modified.m'. The last three, titled
'Problem_1.m', 'Problem_2.m', and 'Problem_3.m' run the first three functions to
the specifications of the problem set and output plots regarding the simulation
performance (accuracy, number of iterations, computation time).

To run the code, use the 'problem' scripts to set the inputs.

For 'problem_1.m' the user can specify a list of numbers corresponding to the
number of iterations the simulation will run. 
For example: iteration_list =
[10, 100, 1000] will run the Monte Carlo simulation three times, first for 10
iterations, then 100, then finally 1000 iterations. Upon completion two plots
will be printed.

For 'problem_2.m' the user can specify a list of desired significant figures
using precision_list. 
For example, set precision_list = [1,2,3] to run the Monte
Carlo Simulation three times until the three specified levels of precision are
achieved. The program will output two plots upon completion.

'problem_3.m' is similar to 'problem_2.m' with the exception that an animation
will show the Monte Carlo simulation during the runtime.

